const chai = require('chai');
const expect = chai.expect;
const app = require('../server.js');
const http = require('chai-http');

chai.use(http);

describe('api_test_suite', () => {

  it("test_api_get_people_is_running", (done) => {
    chai.request('http://localhost:5002')
    .get('/people')
    .end((err, res) => {
    expect(res).to.not.equal(undefined);
    done(); // Call done() to indicate test completion
    });
  });
  
  it('test_api_get_people_returns_200', (done) => {
    chai.request('http://localhost:5002')
    .get('/people')
    .end((err, res) => {
    expect(res.status).to.equal(200);
    done();
    });
  });


  it('test_api_post_person_returns_400_if_no_person_name', (done) => {
    chai.request('http://localhost:5002')
    .post('/users') // Update the endpoint to '/users'
    .type('json')
      .send({
        alias: "James",
        age: 28
      })
    .end((err, res) => {
    expect(res.status).to.equal(400);
    done();
    });
  });


  it('test_api_post_person_returns_400_if_no_ALIAS', (done) => {
    chai.request('http://localhost:5002')
      .post('/users') // Update the endpoint to '/users'
      .type('json')
      .send({
        age: 28
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it('test_api_post_person_returns_400_if_no_AGE', (done) => {
    chai.request('http://localhost:5002')
      .post('/users') // Update the endpoint to '/users'
      .type('json')
      .send({
        name: "James"
      })
      .end((err, res) => {
      expect(res.status).to.equal(400);
      done();
    });
  });
});