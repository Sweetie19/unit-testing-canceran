const names = {
	"Brandon": {
		"name": "Brandon Boyd",
		"age": 35
	},
	"Steve": {
		"name": "Style Tyler",
		"age": 56
	}
}

module.exports = {
	names: names
}
