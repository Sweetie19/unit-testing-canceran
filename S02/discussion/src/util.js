const factorial = (n) => {
	if(typeof n !== 'number'){
		return undefined;}
	else if(n < 0){
		return undefined;
	}
	else if(n === 0){
		return 1;
	}
	else if(n === 1){
		return 1
	}
	else{
		return n * factorial(n-1)
	};
};

module.exports = {
	factorial: factorial
}