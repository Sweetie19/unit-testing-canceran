// util.js

const factorial = (n) => {
  if (typeof n !== 'number') {
    return undefined;
  } else if (n < 0) {
    return undefined;
  } else if (n === 0) {
    return 1;
  } else if (n === 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
};

const div_check = (number) => {
  return number % 5 === 0 || number % 7 === 0;
};


module.exports = {
  factorial: factorial,
  div_check: div_check
};
