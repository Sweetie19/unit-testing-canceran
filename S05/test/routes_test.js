const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');

chai.use(http);

describe('forex_api_test_suite', () => {
    it('test_api_get_rates_is_running', (done) => {
        chai.request('http://localhost:5001')
            .get('/rates')
            .end((err, res) => {
            	expect(res).to.not.equal(undefined);
            done();
        });
    });

   it('test_api_get_rates_returns_200', (done) => {
        chai.request('http://localhost:5001')
            .get('/rates')
            .end((err, res) => {
            	expect(res.status).to.equal(200);
            done();
        });
   });


   it('test_api_get_rates_returns_object_of_size_5', (done) => {
        chai.request('http://localhost:5001')
            .get('/rates')
            .end((err, res) => {
            	expect(Object.keys(res.body.rates)).to.have.length(5);
            done();
		});
	});    	        
    
    it('test_api_post_currency_is_200', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 'usd',
                name: 'United States Dollar',
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
            	expect(res.status).to.equal(200);
            done();
        });
    });

    it('test_api_post_currency_returns_400_if_no_currency_name', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 'usd', 
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
            	expect(res.status).to.equal(400);
            done();
        });
    });

	it('test_api_post_currency_returns_400_if_currency_name_is_not_a_string', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 123, // Providing alias as a number (not a string)
                name: 'United States Dollar',
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
            	expect(res.status).to.equal(400);
            done();
        });
    });


   it('test_api_post_currency_returns_400_if_currency_name_is_an_empty_string', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: '',
                name: 'United States Dollar',
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
            	expect(res.status).to.equal(400);
            done();
        });
    });

    it('test_api_post_currency_returns_400_if_no_currency_ex', () => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 'usd',
                name: 'United States Dollar'
                // Omitting the 'ex' field to simulate missing exchange rates
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            done();
        });
    });

    it('test_api_post_currency_returns_400_if_currency_ex_not_an_object', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 'usd',
                name: 'United States Dollar',
                ex: 'invalid_exchange_rates' // Providing a string instead of an object
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            done();
        });
    });

    it('test_api_post_currency_returns_400_if_no_ex_content', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 'usd',
                name: 'United States Dollar',
                // Omitting the 'ex' field to simulate missing exchange rates
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            done();
        });
    });

    it('test_api_post_currency_returns_400_if_no_currency_alias', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                // Omitting the 'alias' field to simulate missing currency alias
                name: 'United States Dollar',
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            done();
        });
 	});

    it('test_api_post_currency_returns_400_if_currency_alias_not_a_string', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 123, // Providing alias as a number (not a string)
                name: 'United States Dollar',
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            done();
        });
    });

    it('test_api_post_currency_returns_400_if_currency_alias_is_an_empty_string', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: '', // Providing an empty string as the currency alias
                name: 'United States Dollar',
                ex: {
                    'peso': 0.47,
                    'usd': 0.0092,
                    'won': 10.93,
                    'yuan': 0.065
                }
            })
            .end((err, res) => {
                expect(res.status).to.equal(400);
            done();
        });
    });

	it('test_api_post_currency_returns_400_if_duplicate_alias_found', () => {
        // First, create a currency with the same alias to ensure a duplicate alias
    	chai.request('http://localhost:5001')
        	.post('/currency')
        	.type('json')
        	.send({
           	alias: 'usd',
            name: 'United States Dollar',
            ex: {
                'peso': 0.47,
                'usd': 0.0092,
                'won': 10.93,
                'yuan': 0.065
            }
        })
        .end(() => {
                // Attempt to create another currency with the same alias
            chai.request('http://localhost:5001')
                .post('/currency')
                .type('json')
                .send({
                    alias: 'usd', // Providing a duplicate alias
                    name: 'Euro',
                    ex: {
                        'euro': 0.85,
                        'usd': 1.18
                    }
                })
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                done();
            });
        });
    });


  	it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
        chai.request('http://localhost:5001')
            .post('/currency')
            .type('json')
            .send({
                alias: 'new_currency',
                name: 'New Currency',
                ex: {
                    'peso': 1.0,
                    'usd': 2.0,
                    'won': 3.0,
                    'yuan': 4.0
                }
            })
            .end((err, res) => {
            	expect(res.status).to.equal(200);
            done(); 
        });
    });

    it('test_api_post_currency_returns_submitted_object_to_show_submission_was_written', () => {
    	chai.request('http://localhost:5001')
        	.post('/currency')
        	.type('json')
        	.send({
            	alias: 'new_currency',
            	name: 'New Currency',
            	ex: {
                	'peso': 1.0,
                	'usd': 2.0,
                	'won': 3.0,
                	'yuan': 4.0
            	}
        	})
        	.end((err, res) => {
           		expect(res.status).to.equal(200);
            	expect(res.body.message).to.equal('Currency submitted successfully');
            	expect(res.body.submittedCurrency).to.deep.equal({
                	alias: 'new_currency',
                	name: 'New Currency',
                	ex: {
                    	'peso': 1.0,
                    	'usd': 2.0,
                    	'won': 3.0,
                    	'yuan': 4.0
                	}
            	});
            done();
        });
	});
});



