const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({ 'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	});

	app.post('/currency', (req, res) => {
		const { name, ex, alias } = req.body;
		// no currency name
		if (!name) {
            return res.status(400).json({ error: 'Currency name is missing' });
        }
		// not a string
		if (typeof name !== 'string') {
            return res.status(400).json({ error: 'Currency name is not a string' });
        }
		// empty string
		const emptyCurrencyNames = Object.keys(ex).filter(currency => ex[currency] === '');
        if (emptyCurrencyNames.length > 0) {
            return res.status(400).json({ error: 'Currency name(s) cannot be empty string' });
        }
		// no currency ex
		if (!ex) {
            return res.status(400).json({ error: 'Currency ex is missing' });
        }
		// currency ex not an object
		if (!ex || typeof ex !== 'object') {
            return res.status(400).json({ error: 'Currency ex should be an object' });
        }
		// no ex content
		if (Object.keys(ex).length === 0) {
            return res.status(400).json({ error: 'Currency ex has no content' });
        }
		// no currency alias
		if (Object.keys(ex).length === 0) {
        	return res.status(400).json({ error: 'Currency ex has no content' });
    	};
		// alias is not a string
		if (!alias || typeof alias !== 'string' || alias === '') {
        	return res.status(400).json({ error: 'Currency alias is missing or invalid' });
    	};
		//  alias is empty
		//  duplicate alias
		const duplicateAlias = Object.values(exchangeRates).find(currency => currency.alias === alias);
    	if (duplicateAlias) {
        	return res.status(400).json({ error: 'Currency alias is already used' });
    	}
    		exchangeRates[alias] = { name, ex };
        res.status(200).json({ message: 'Currency submitted successfully' });
        return res.status(200).send({ rates: exchangeRates });
    });
};
	